The Project Gutenberg EBook of The Man from Snowy River, by
Andrew Barton 'Banjo' Paterson

This eBook is for the use of anyone anywhere at no cost and with
almost no restrictions whatsoever.  You may copy it, give it away or
re-use it under the terms of the Project Gutenberg License included
with this eBook or online at www.gutenberg.org



Title: The Man from Snowy River

Author: Andrew Barton 'Banjo' Paterson

Posting Date: July 11, 2008 [EBook #213]
Release Date: February, 1995
Last Updated: March 9, 2018

Language: English

Character set encoding: UTF-8

*** START OF THIS PROJECT GUTENBERG EBOOK THE MAN FROM SNOWY RIVER ***




Produced by A. Light, and Sheridan Ash





THE MAN FROM SNOWY RIVER AND OTHER VERSES

(Second edition)

by Andrew Barton 'Banjo' Paterson

[Australian Poet, Reporter -- 1864-1941.]


[Note on text:  Italicized stanzas will be indented 5 spaces.
Italicized words or phrases will be capitalized.
Lines longer than 75 characters have been broken according to metre,
and the continuation is indented two spaces.  Also,
some obvious errors, after being confirmed against other sources,
have been corrected.]


[Note on content:  Banjo Paterson and Henry Lawson were writing for
the Sydney 'Bulletin' in 1892 when Lawson suggested a 'duel' of poetry
to increase the number of poems they could sell to the paper.
It was apparently entered into in all fun, though there are reports
that Lawson was bitter about it later.  'In Defence of the Bush',
included in this selection, was one of Paterson's replies to Lawson.]


[The 1913 printing (Sydney, Fifty-third Thousand) of the Second Edition
(first published in 1902) was used in the preparation of this etext.
First edition was first published in 1895.]





THE MAN FROM SNOWY RIVER AND OTHER VERSES

by A. B. Paterson (“The Banjo”)

with preface by Rolf Boldrewood





Preface



It is not so easy to write ballads descriptive of the bushland of Australia
as on light consideration would appear.  Reasonably good verse
on the subject has been supplied in sufficient quantity.
But the maker of folksongs for our newborn nation requires
a somewhat rare combination of gifts and experiences.
Dowered with the poet's heart, he must yet have passed his 'wander-jaehre'
amid the stern solitude of the Austral waste -- must have ridden the race
in the back-block township, guided the reckless stock-horse
adown the mountain spur, and followed the night-long moving,
spectral-seeming herd 'in the droving days'.  Amid such scarce
congenial surroundings comes oft that finer sense which renders visible
bright gleams of humour, pathos, and romance, which,
like undiscovered gold, await the fortunate adventurer.
That the author has touched this treasure-trove, not less delicately
than distinctly, no true Australian will deny.  In my opinion
this collection comprises the best bush ballads written
since the death of Lindsay Gordon.

Rolf Boldrewood


A number of these verses are now published for the first time,
most of the others were written for and appeared in “The Bulletin”
 (Sydney, N.S.W.), and are therefore already widely known
to readers in Australasia.

A. B. Paterson







Prelude



     I have gathered these stories afar,
      In the wind and the rain,
     In the land where the cattle camps are,
      On the edge of the plain.
     On the overland routes of the west,
      When the watches were long,
     I have fashioned in earnest and jest
      These fragments of song.

     They are just the rude stories one hears
      In sadness and mirth,
     The records of wandering years,
      And scant is their worth
     Though their merits indeed are but slight,
      I shall not repine,
     If they give you one moment's delight,
      Old comrades of mine.






Contents with First Lines:



     Prelude
        I have gathered these stories afar,

     The Man from Snowy River
        There was movement at the station, for the word had passed around

     Old Pardon, the Son of Reprieve
        You never heard tell of the story?

     Clancy of the Overflow
        I had written him a letter which I had, for want of better

     Conroy's Gap
        This was the way of it, don't you know --

     Our New Horse
        The boys had come back from the races

     An Idyll of Dandaloo
        On Western plains, where shade is not,

     The Geebung Polo Club
        It was somewhere up the country, in a land of rock and scrub,

     The Travelling Post Office
        The roving breezes come and go, the reed beds sweep and sway,

     Saltbush Bill
        Now this is the law of the Overland that all in the West obey,

     A Mountain Station
        I bought a run a while ago,

     Been There Before
        There came a stranger to Walgett town,

     The Man Who Was Away
        The widow sought the lawyer's room with children three in tow,

     The Man from Ironbark
        It was the man from Ironbark who struck the Sydney town,

     The Open Steeplechase
        I had ridden over hurdles up the country once or twice,

     The Amateur Rider
        _HIM_ going to ride for us!  _HIM_ --
          with the pants and the eyeglass and all.

     On Kiley's Run
        The roving breezes come and go

     Frying Pan's Theology
        Scene:  On Monaro.

     The Two Devines
        It was shearing-time at the Myall Lake,

     In the Droving Days
        'Only a pound,' said the auctioneer,

     Lost
        'He ought to be home,' said the old man,
          'without there's something amiss.

     Over the Range
        Little bush maiden, wondering-eyed,

     Only a Jockey
        Out in the grey cheerless chill of the morning light,

     How M'Ginnis Went Missing
        Let us cease our idle chatter,

     A Voice from the Town
        I thought, in the days of the droving,

     A Bunch of Roses
        Roses ruddy and roses white,

     Black Swans
        As I lie at rest on a patch of clover

     The All Right 'Un
        He came from 'further out',

The Boss of the 'Admiral Lynch'
        Did you ever hear tell of Chili?  I was readin' the other day

     A Bushman's Song
        I'm travellin' down the Castlereagh, and I'm a station hand,

     How Gilbert Died
        There's never a stone at the sleeper's head,

     The Flying Gang
        I served my time, in the days gone by,

     Shearing at Castlereagh
        The bell is set a-ringing, and the engine gives a toot,

     The Wind's Message
        There came a whisper down the Bland between the dawn and dark,

     Johnson's Antidote
        Down along the Snakebite River, where the overlanders camp,

     Ambition and Art
        I am the maid of the lustrous eyes

     The Daylight is Dying
        The daylight is dying

     In Defence of the Bush
        So you're back from up the country, Mister Townsman, where you went,

     Last Week
        Oh, the new-chum went to the back block run,

     Those Names
        The shearers sat in the firelight, hearty and hale and strong,

     A Bush Christening
        On the outer Barcoo where the churches are few,

     How the Favourite Beat Us
        'Aye,' said the boozer, 'I tell you it's true, sir,

     The Great Calamity
        MacFierce'un came to Whiskeyhurst

     Come-by-Chance
        As I pondered very weary o'er a volume long and dreary --

     Under the Shadow of Kiley's Hill
        This is the place where they all were bred;

     Jim Carew
        Born of a thoroughbred English race,

     The Swagman's Rest
        We buried old Bob where the bloodwoods wave





THE MAN FROM SNOWY RIVER AND OTHER VERSES



The Man from Snowy River



   There was movement at the station, for the word had passed around
    That the colt from old Regret had got away,
   And had joined the wild bush horses -- he was worth a thousand pound,
    So all the cracks had gathered to the fray.
   All the tried and noted riders from the stations near and far
    Had mustered at the homestead overnight,
   For the bushmen love hard riding where the wild bush horses are,
    And the stock-horse snuffs the battle with delight.

   There was Harrison, who made his pile when Pardon won the cup,
    The old man with his hair as white as snow;
   But few could ride beside him when his blood was fairly up --
    He would go wherever horse and man could go.
   And Clancy of the Overflow came down to lend a hand,
    No better horseman ever held the reins;
   For never horse could throw him while the saddle-girths would stand,
    He learnt to ride while droving on the plains.

   And one was there, a stripling on a small and weedy beast,
    He was something like a racehorse undersized,
   With a touch of Timor pony -- three parts thoroughbred at least --
    And such as are by mountain horsemen prized.
   He was hard and tough and wiry -- just the sort that won't say die --
    There was courage in his quick impatient tread;
   And he bore the badge of gameness in his bright and fiery eye,
    And the proud and lofty carriage of his head.

   But still so slight and weedy, one would doubt his power to stay,
    And the old man said, 'That horse will never do
   For a long and tiring gallop -- lad, you'd better stop away,
    Those hills are far too rough for such as you.'
   So he waited sad and wistful -- only Clancy stood his friend --
    'I think we ought to let him come,' he said;
   'I warrant he'll be with us when he's wanted at the end,
    For both his horse and he are mountain bred.

   'He hails from Snowy River, up by Kosciusko's side,
    Where the hills are twice as steep and twice as rough,
   Where a horse's hoofs strike firelight from the flint stones every stride,
    The man that holds his own is good enough.
   And the Snowy River riders on the mountains make their home,
    Where the river runs those giant hills between;
   I have seen full many horsemen since I first commenced to roam,
    But nowhere yet such horsemen have I seen.'

   So he went -- they found the horses by the big mimosa clump --
    They raced away towards the mountain's brow,
   And the old man gave his orders, 'Boys, go at them from the jump,
    No use to try for fancy riding now.
   And, Clancy, you must wheel them, try and wheel them to the right.
    Ride boldly, lad, and never fear the spills,
   For never yet was rider that could keep the mob in sight,
    If once they gain the shelter of those hills.'

   So Clancy rode to wheel them -- he was racing on the wing
    Where the best and boldest riders take their place,
   And he raced his stock-horse past them, and he made the ranges ring
    With the stockwhip, as he met them face to face.
   Then they halted for a moment, while he swung the dreaded lash,
    But they saw their well-loved mountain full in view,
   And they charged beneath the stockwhip with a sharp and sudden dash,
    And off into the mountain scrub they flew.

   Then fast the horsemen followed, where the gorges deep and black
    Resounded to the thunder of their tread,
   And the stockwhips woke the echoes, and they fiercely answered back
    From cliffs and crags that beetled overhead.
   And upward, ever upward, the wild horses held their way,
    Where mountain ash and kurrajong grew wide;
   And the old man muttered fiercely, 'We may bid the mob good day,
    _NO_ man can hold them down the other side.'

   When they reached the mountain's summit, even Clancy took a pull,
    It well might make the boldest hold their breath,
   The wild hop scrub grew thickly, and the hidden ground was full
    Of wombat holes, and any slip was death.
   But the man from Snowy River let the pony have his head,
    And he swung his stockwhip round and gave a cheer,
   And he raced him down the mountain like a torrent down its bed,
    While the others stood and watched in very fear.

   He sent the flint stones flying, but the pony kept his feet,
    He cleared the fallen timber in his stride,
   And the man from Snowy River never shifted in his seat --
    It was grand to see that mountain horseman ride.
   Through the stringy barks and saplings, on the rough and broken ground,
    Down the hillside at a racing pace he went;
   And he never drew the bridle till he landed safe and sound,
    At the bottom of that terrible descent.

   He was right among the horses as they climbed the further hill,
    And the watchers on the mountain standing mute,
   Saw him ply the stockwhip fiercely, he was right among them still,
    As he raced across the clearing in pursuit.
   Then they lost him for a moment, where two mountain gullies met
    In the ranges, but a final glimpse reveals
   On a dim and distant hillside the wild horses racing yet,
    With the man from Snowy River at their heels.

   And he ran them single-handed till their sides were white with foam.
    He followed like a bloodhound on their track,
   Till they halted cowed and beaten, then he turned their heads for home,
    And alone and unassisted brought them back.
   But his hardy mountain pony he could scarcely raise a trot,
    He was blood from hip to shoulder from the spur;
   But his pluck was still undaunted, and his courage fiery hot,
    For never yet was mountain horse a cur.
	
	
   And down by Kosciusko, where the pine-clad ridges raise
    Their torn and rugged battlements on high,
   Where the air is clear as crystal, and the white stars fairly blaze
    At midnight in the cold and frosty sky,
   And where around the Overflow the reedbeds sweep and sway
    To the breezes, and the rolling plains are wide,
   The man from Snowy River is a household word to-day,
    And the stockmen tell the story of his ride.




Old Pardon, the Son of Reprieve



   You never heard tell of the story?
    Well, now, I can hardly believe!
   Never heard of the honour and glory
    Of Pardon, the son of Reprieve?
   But maybe you're only a Johnnie
    And don't know a horse from a hoe?
   Well, well, don't get angry, my sonny,
    But, really, a young un should know.

   They bred him out back on the 'Never',
    His mother was Mameluke breed.
   To the front -- and then stay there -- was ever
    The root of the Mameluke creed.
   He seemed to inherit their wiry
    Strong frames -- and their pluck to receive --
   As hard as a flint and as fiery
    Was Pardon, the son of Reprieve.

   We ran him at many a meeting
    At crossing and gully and town,
   And nothing could give him a beating --
    At least when our money was down.
   For weight wouldn't stop him, nor distance,
    Nor odds, though the others were fast,
   He'd race with a dogged persistence,
    And wear them all down at the last.

   At the Turon the Yattendon filly
    Led by lengths at the mile-and-a-half,
   And we all began to look silly,
    While _HER_ crowd were starting to laugh;
   But the old horse came faster and faster,
    His pluck told its tale, and his strength,
   He gained on her, caught her, and passed her,
    And won it, hands-down, by a length.

   And then we swooped down on Menindie
    To run for the President's Cup --
   Oh! that's a sweet township -- a shindy
    To them is board, lodging, and sup.
   Eye-openers they are, and their system
    Is never to suffer defeat;
   It's 'win, tie, or wrangle' -- to best 'em
    You must lose 'em, or else it's 'dead heat'.

   We strolled down the township and found 'em
    At drinking and gaming and play;
   If sorrows they had, why they drowned 'em,
    And betting was soon under way.
   Their horses were good 'uns and fit 'uns,
    There was plenty of cash in the town;
   They backed their own horses like Britons,
    And, Lord! how _WE_ rattled it down!

   With gladness we thought of the morrow,
    We counted our wagers with glee,
   A simile homely to borrow --
    'There was plenty of milk in our tea.'
   You see we were green; and we never
    Had even a thought of foul play,
   Though we well might have known that the clever
    Division would 'put us away'.

   Experience 'docet', they tell us,
    At least so I've frequently heard,
   But, 'dosing' or 'stuffing', those fellows
    Were up to each move on the board:
   They got to his stall -- it is sinful
    To think what such villains would do --
   And they gave him a regular skinful
    Of barley -- green barley -- to chew.

   He munched it all night, and we found him
    Next morning as full as a hog --
   The girths wouldn't nearly meet round him;
    He looked like an overfed frog.
   We saw we were done like a dinner --
    The odds were a thousand to one
   Against Pardon turning up winner,
    'Twas cruel to ask him to run.

   We got to the course with our troubles,
    A crestfallen couple were we;
   And we heard the 'books' calling the doubles --
    A roar like the surf of the sea;
   And over the tumult and louder
    Rang 'Any price Pardon, I lay!'
   Says Jimmy, 'The children of Judah
    Are out on the warpath to-day.'

   Three miles in three heats: -- Ah, my sonny,
    The horses in those days were stout,
   They had to run well to win money;
    I don't see such horses about.
   Your six-furlong vermin that scamper
    Half-a-mile with their feather-weight up;
   They wouldn't earn much of their damper
    In a race like the President's Cup.

   The first heat was soon set a-going;
    The Dancer went off to the front;
   The Don on his quarters was showing,
    With Pardon right out of the hunt.
   He rolled and he weltered and wallowed --
    You'd kick your hat faster, I'll bet;
   They finished all bunched, and he followed
    All lathered and dripping with sweat.

   But troubles came thicker upon us,
    For while we were rubbing him dry
   The stewards came over to warn us:
    'We hear you are running a bye!
   If Pardon don't spiel like tarnation
    And win the next heat -- if he can --
   He'll earn a disqualification;
    Just think over _THAT_, now, my man!'

   Our money all gone and our credit,
    Our horse couldn't gallop a yard;
   And then people thought that _WE_ did it!
    It really was terribly hard.
   We were objects of mirth and derision
    To folk in the lawn and the stand,
   And the yells of the clever division
    Of 'Any price Pardon!' were grand.

   We still had a chance for the money,
    Two heats still remained to be run;
   If both fell to us -- why, my sonny,
    The clever division were done.
   And Pardon was better, we reckoned,
    His sickness was passing away,
   So he went to the post for the second
    And principal heat of the day.

   They're off and away with a rattle,
    Like dogs from the leashes let slip,
   And right at the back of the battle
    He followed them under the whip.
   They gained ten good lengths on him quickly
    He dropped right away from the pack;
   I tell you it made me feel sickly
    To see the blue jacket fall back.

   Our very last hope had departed --
    We thought the old fellow was done,
   When all of a sudden he started
    To go like a shot from a gun.
   His chances seemed slight to embolden
    Our hearts; but, with teeth firmly set,
   We thought, 'Now or never!  The old 'un
    May reckon with some of 'em yet.'

   Then loud rose the war-cry for Pardon;
    He swept like the wind down the dip,
   And over the rise by the garden,
    The jockey was done with the whip
   The field were at sixes and sevens --
    The pace at the first had been fast --
   And hope seemed to drop from the heavens,
    For Pardon was coming at last.

   And how he did come!  It was splendid;
    He gained on them yards every bound,
   Stretching out like a greyhound extended,
    His girth laid right down on the ground.
   A shimmer of silk in the cedars
    As into the running they wheeled,
   And out flashed the whips on the leaders,
    For Pardon had collared the field.	

Conroy's Gap



   This was the way of it, don't you know --
    Ryan was 'wanted' for stealing sheep,
   And never a trooper, high or low,
    Could find him -- catch a weasel asleep!
   Till Trooper Scott, from the Stockman's Ford --
    A bushman, too, as I've heard them tell --
   Chanced to find him drunk as a lord
    Round at the Shadow of Death Hotel.

   D'you know the place?  It's a wayside inn,
    A low grog-shanty -- a bushman trap,
   Hiding away in its shame and sin
    Under the shelter of Conroy's Gap --
   Under the shade of that frowning range,
    The roughest crowd that ever drew breath --
   Thieves and rowdies, uncouth and strange,
    Were mustered round at the Shadow of Death.

   The trooper knew that his man would slide
    Like a dingo pup, if he saw the chance;
   And with half a start on the mountain side
    Ryan would lead him a merry dance.
   Drunk as he was when the trooper came,
    To him that did not matter a rap --
   Drunk or sober, he was the same,
    The boldest rider in Conroy's Gap.

   'I want you, Ryan,' the trooper said,
    'And listen to me, if you dare resist,
   So help me heaven, I'll shoot you dead!'
    He snapped the steel on his prisoner's wrist,
   And Ryan, hearing the handcuffs click,
    Recovered his wits as they turned to go,
   For fright will sober a man as quick
    As all the drugs that the doctors know.

   There was a girl in that rough bar
    Went by the name of Kate Carew,
   Quiet and shy as the bush girls are,
    But ready-witted and plucky, too.
   She loved this Ryan, or so they say,
    And passing by, while her eyes were dim
   With tears, she said in a careless way,
    'The Swagman's round in the stable, Jim.'

   Spoken too low for the trooper's ear,
    Why should she care if he heard or not?
   Plenty of swagmen far and near,
    And yet to Ryan it meant a lot.
   That was the name of the grandest horse
    In all the district from east to west
   In every show ring, on every course
    They always counted the Swagman best.

   He was a wonder, a raking bay --
    One of the grand old Snowdon strain --
   One of the sort that could race and stay
    With his mighty limbs and his length of rein.
   Born and bred on the mountain side,
    He could race through scrub like a kangaroo,
   The girl herself on his back might ride,
    And the Swagman would carry her safely through.

   He would travel gaily from daylight's flush
    Till after the stars hung out their lamps,
   There was never his like in the open bush,
    And never his match on the cattle-camps.
   For faster horses might well be found
    On racing tracks, or a plain's extent,
   But few, if any, on broken ground
    Could see the way that the Swagman went.

   When this girl's father, old Jim Carew,
    Was droving out on the Castlereagh
   With Conroy's cattle, a wire came through
    To say that his wife couldn't live the day.
   And he was a hundred miles from home,
    As flies the crow, with never a track,
   Through plains as pathless as ocean's foam,
    He mounted straight on the Swagman's back.

   He left the camp by the sundown light,
    And the settlers out on the Marthaguy
   Awoke and heard, in the dead of night,
    A single horseman hurrying by.
   He crossed the Bogan at Dandaloo,
    And many a mile of the silent plain
   That lonely rider behind him threw
    Before they settled to sleep again.

   He rode all night and he steered his course
    By the shining stars with a bushman's skill,
   And every time that he pressed his horse
    The Swagman answered him gamely still.
   He neared his home as the east was bright,
    The doctor met him outside the town:
   'Carew!  How far did you come last night?'
    'A hundred miles since the sun went down.'

   And his wife got round, and an oath he passed,
    So long as he or one of his breed
   Could raise a coin, though it took their last
    The Swagman never should want a feed.
   And Kate Carew, when her father died,
    She kept the horse and she kept him well:
   The pride of the district far and wide,
    He lived in style at the bush hotel.

   Such was the Swagman; and Ryan knew
    Nothing about could pace the crack;
   Little he'd care for the man in blue
    If once he got on the Swagman's back.
   But how to do it?  A word let fall
    Gave him the hint as the girl passed by;
   Nothing but 'Swagman -- stable-wall;
    'Go to the stable and mind your eye.'

   He caught her meaning, and quickly turned
    To the trooper:  'Reckon you'll gain a stripe
   By arresting me, and it's easily earned;
    Let's go to the stable and get my pipe,
   The Swagman has it.'  So off they went,
    And soon as ever they turned their backs
   The girl slipped down, on some errand bent
    Behind the stable, and seized an axe.

   The trooper stood at the stable door
    While Ryan went in quite cool and slow,
   And then (the trick had been played before)
    The girl outside gave the wall a blow.
   Three slabs fell out of the stable wall --
    'Twas done 'fore ever the trooper knew --
   And Ryan, as soon as he saw them fall,
    Mounted the Swagman and rushed him through.

   The trooper heard the hoof-beats ring
    In the stable yard, and he slammed the gate,
   But the Swagman rose with a mighty spring
    At the fence, and the trooper fired too late,
   As they raced away and his shots flew wide
    And Ryan no longer need care a rap,
   For never a horse that was lapped in hide
    Could catch the Swagman in Conroy's Gap.

   And that's the story.  You want to know
    If Ryan came back to his Kate Carew;
   Of course he should have, as stories go,
    But the worst of it is, this story's true:
   And in real life it's a certain rule,
    Whatever poets and authors say
   Of high-toned robbers and all their school,
    These horsethief fellows aren't built that way.

   Come back!  Don't hope it -- the slinking hound,
    He sloped across to the Queensland side,
   And sold the Swagman for fifty pound,
    And stole the money, and more beside.
   And took to drink, and by some good chance
    Was killed -- thrown out of a stolen trap.
   And that was the end of this small romance,
    The end of the story of Conroy's Gap.
Our New Horse



   The boys had come back from the races
    All silent and down on their luck;
   They'd backed 'em, straight out and for places,
    But never a winner they struck.
   They lost their good money on Slogan,
    And fell, most uncommonly flat,
   When Partner, the pride of the Bogan,
    Was beaten by Aristocrat.

   And one said, 'I move that instanter
    We sell out our horses and quit,
   The brutes ought to win in a canter,
    Such trials they do when they're fit.
   The last one they ran was a snorter --
    A gallop to gladden one's heart --
   Two-twelve for a mile and a quarter,
    And finished as straight as a dart.

   'And then when I think that they're ready
    To win me a nice little swag,
   They are licked like the veriest neddy --
    They're licked from the fall of the flag.
   The mare held her own to the stable,
    She died out to nothing at that,
   And Partner he never seemed able
    To pace it with Aristocrat.

   'And times have been bad, and the seasons
    Don't promise to be of the best;
   In short, boys, there's plenty of reasons
    For giving the racing a rest.
   The mare can be kept on the station --
    Her breeding is good as can be --
   But Partner, his next destination
    Is rather a trouble to me.

   'We can't sell him here, for they know him
    As well as the clerk of the course;
   He's raced and won races till, blow him,
    He's done as a handicap horse.
   A jady, uncertain performer,
    They weight him right out of the hunt,
   And clap it on warmer and warmer
    Whenever he gets near the front.

   'It's no use to paint him or dot him
    Or put any 'fake' on his brand,
   For bushmen are smart, and they'd spot him
    In any sale-yard in the land.
   The folk about here could all tell him,
    Could swear to each separate hair;
   Let us send him to Sydney and sell him,
    There's plenty of Jugginses there.

   'We'll call him a maiden, and treat 'em
    To trials will open their eyes,
   We'll run their best horses and beat 'em,
    And then won't they think him a prize.
   I pity the fellow that buys him,
    He'll find in a very short space,
   No matter how highly he tries him,
    The beggar won't _RACE_ in a race.'

       .   .   .   .   .
	   
	    Next week, under 'Seller and Buyer',
    Appeared in the _DAILY GAZETTE_:
   'A racehorse for sale, and a flyer;
    Has never been started as yet;
   A trial will show what his pace is;
    The buyer can get him in light,
   And win all the handicap races.
    Apply here before Wednesday night.'

   He sold for a hundred and thirty,
    Because of a gallop he had
   One morning with Bluefish and Bertie,
    And donkey-licked both of 'em bad.
   And when the old horse had departed,
    The life on the station grew tame;
   The race-track was dull and deserted,
    The boys had gone back on the game.

       .   .   .   .   .

   The winter rolled by, and the station
    Was green with the garland of spring
   A spirit of glad exultation
    Awoke in each animate thing.
   And all the old love, the old longing,
    Broke out in the breasts of the boys,
   The visions of racing came thronging
    With all its delirious joys.

   The rushing of floods in their courses,
    The rattle of rain on the roofs
   Recalled the fierce rush of the horses,
    The thunder of galloping hoofs.
   And soon one broke out:  'I can suffer
    No longer the life of a slug,
   The man that don't race is a duffer,
    Let's have one more run for the mug.

   'Why, _EVERYTHING_ races, no matter
    Whatever its method may be:
   The waterfowl hold a regatta;
    The 'possums run heats up a tree;
   The emus are constantly sprinting
    A handicap out on the plain;
   It seems like all nature was hinting,
    'Tis time to be at it again.

   'The cockatoo parrots are talking
    Of races to far away lands;
   The native companions are walking
    A go-as-you-please on the sands;
   The little foals gallop for pastime;
    The wallabies race down the gap;
   Let's try it once more for the last time,
    Bring out the old jacket and cap.

   'And now for a horse; we might try one
    Of those that are bred on the place,
   But I think it better to buy one,
    A horse that has proved he can race.
   Let us send down to Sydney to Skinner,
    A thorough good judge who can ride,
   And ask him to buy us a spinner
    To clean out the whole countryside.'

   They wrote him a letter as follows:
    'We want you to buy us a horse;
   He must have the speed to catch swallows,
    And stamina with it of course.
   The price ain't a thing that'll grieve us,
    It's getting a bad 'un annoys
   The undersigned blokes, and believe us,
    We're yours to a cinder, 'the boys'.'

   He answered:  'I've bought you a hummer,
    A horse that has never been raced;
   I saw him run over the Drummer,
    He held him outclassed and outpaced.
   His breeding's not known, but they state he
    Is born of a thoroughbred strain,
   I paid them a hundred and eighty,
    And started the horse in the train.'

   They met him -- alas, that these verses
    Aren't up to the subject's demands --
   Can't set forth their eloquent curses,
    _FOR PARTNER WAS BACK ON THEIR HANDS_.
   They went in to meet him in gladness,
    They opened his box with delight --
   A silent procession of sadness
    They crept to the station at night.

   And life has grown dull on the station,
    The boys are all silent and slow;
   Their work is a daily vexation,
    And sport is unknown to them now.
   Whenever they think how they stranded,
    They squeal just like guinea-pigs squeal;
   They bit their own hook, and were landed
    With fifty pounds loss on the deal.


